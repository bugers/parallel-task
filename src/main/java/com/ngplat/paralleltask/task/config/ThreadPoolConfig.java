/**
 * @project: parallel-task
 * @package: com.ngplat.paralleltask.task.config
 * @filename: ThreadPoolConfig.java
 *
 * Copyright (c) 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 */
package com.ngplat.paralleltask.task.config;

/** 
 * @typename: ThreadPoolConfig
 * @brief: 获取线程池配置参数接口
 * @author: KI ZCQ
 * @date: 2018年4月23日 下午4:59:22
 * @version: 1.0.0
 * @since
 * 
 */
public interface ThreadPoolConfig extends ThreadConfig {

	/**
	 * @Description: 获取任务超时时间(单位: 毫秒) 
	 * @return
	 */
	long getTaskTimeoutMillSeconds();
	
	/**
	 * @Description: 当任务队列满时的等待时间 
	 * @return
	 */
    int getQueueFullSleepTime();
    
    /**
     * @Description: 获取最大可缓存任务数  
     * @return
     */
    int getMaxCacheTaskNum();
    
    /**
     * @Description: 获取核心任务数  
     * @return
     */
    int getCoreTaskNum();
    
    /**
     * @Description: 获取最大任务数 
     * @return
     */
    int getMaxTaskNum();
	
}
