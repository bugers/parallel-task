/**
 * @project: parallel-task
 * @package: com.ngplat.paralleltask.task.event
 * @filename: WorkerListener.java
 *
 * Copyright (c) 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 */
package com.ngplat.paralleltask.task.event;

import com.ngplat.paralleltask.constants.TaskState;

/** 
 * @typename: WorkerListener
 * @brief: 任务监听器
 * @author: KI ZCQ
 * @date: 2018年5月14日 上午10:32:07
 * @version: 1.0.0
 * @since
 * 
 */
public interface WorkerListener {

	/**
	 * @Description: 任务完成后通知 
	 * @param taskKey
	 * @param state
	 */
	public void onTaskComplete(String taskKey, TaskState state);

}
