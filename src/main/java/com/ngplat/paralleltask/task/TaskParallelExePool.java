/**
 * @project: parallel-task
 * @package: com.ngplat.paralleltask.task
 * @filename: TaskParallelExePool.java
 *
 * Copyright (c) 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 */
package com.ngplat.paralleltask.task;

import com.ngplat.paralleltask.exception.TaskExecutionException;

/** 
 * @typename: TaskParallelExePool
 * @brief: 任务执行方法
 * @author: KI ZCQ
 * @date: 2018年5月29日 上午10:08:36
 * @version: 1.0.0
 * @since
 * 
 */
public interface TaskParallelExePool {

	/**
	 * @Description: 真正执行业务逻辑的地方  
	 * @param context
	 * @throws TaskExecutionException
	 */
	public void runWorker(TaskContext context) throws TaskExecutionException;

	/**
	 * @Description: 开始执行前的处理
	 * @param context
	 */
	public void beforeExecute(TaskContext context);

	/**
	 * @Description: 任务执行后的处理
	 * @param context
	 */
	public void afterExecute(TaskContext context);
	
}
