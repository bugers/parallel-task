/**
 * @project: parallel-task
 * @package: com.ngplat.paralleltask.constants
 * @filename: TaskState.java
 *
 * Copyright (c) 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 */
package com.ngplat.paralleltask.constants;

/** 
 * @typename: TaskState
 * @brief: 任务执行状态
 * @author: KI ZCQ
 * @date: 2018年5月10日 下午9:22:01
 * @version: 1.0.0
 * @since
 * 
 */
public enum TaskState {
	// 初始化状态
	TASK_INITIAL,
	// 任务执行中
	TASK_RUNNING,
	// 执行完成
	TASK_COMPLETE,
	// 执行结束
    TASK_FAILED;
}
