/**
 * @project: parallel-task
 * @package: com.ngplat.paralleltask.annotation
 * @filename: TaskBean.java
 *
 * Copyright (c) 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 */
package com.ngplat.paralleltask.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.stereotype.Component;

import com.ngplat.paralleltask.constants.TaskConsts;
import com.ngplat.paralleltask.model.JobInfo;

/** 
 * @typename: TaskBean
 * @brief: 任务注解
 * @date: 2018年5月10日 上午9:49:45
 * @version: 1.0.0
 * @since
 * 
 */
@Component
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface TaskBean {
	
	/**
	 * @Description: 任务Id, 必填项  
	 * @return
	 */
	String taskId();
	
	/**
	 * @Description: 一组任务Id, 非必填项 
	 * @return
	 */
	String jobId() default JobInfo.DEFAULT_GROUP_ID;
	
	/**
	 * @Description: 任务名称, 非必填项
	 */
    String name() default TaskConsts.DEFAULT_TASK_NAME;
	
	/**
	 * @Description: 依赖父节点, 非必填项, 默认父节点是根节点
	 */
	String[] parentIds() default { TaskConsts.ROOT_TASK_ID };
	
	/**
	 * @Description: 任务状态, 非必填项, 默认是启用状态
	 */
	String status() default TaskConsts.ENABLE_STATUS;
	
	/** 
     * @Description: 优先级, 非必填项
     * @return 
     */  
    int priority() default TaskConsts.LOWEST_PRECEDENCE;
	
}
